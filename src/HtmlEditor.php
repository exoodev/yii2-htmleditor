<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\htmleditor;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * HTML editor
 *
 * For example to use the timepicker with a [[\yii\base\Model|model]]:
 *
 * ```php
 * echo HtmlEditor::widget([
 *     'model' => $model,
 *     'attribute' => 'text',
 * ]);
 * ```
 *
 * The following example will use the name property instead:
 *
 * ```php
 * echo HtmlEditor::widget([
 *     'name'  => 'text',
 *     'value'  => $value,
 * ]);
 * ```
 *
 * You can also use this widget in an [[\yii\widgets\ActiveForm|ActiveForm]] using the [[\yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'text')->widget(HtmlEditor::classname(), [
 *     'clientOptions' => ['lineNumbers' => true],
 * ]) ?>
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class HtmlEditor extends InputWidget
{
    /**
     * @var array the options for the underlying JS plugin.
     */
    public $clientOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $clientOptions = Json::encode($this->clientOptions);
        $id = $this->options['id'];
        $view = $this->getView();
        $view->registerJs("$('#$id').htmlEditor($clientOptions);");
        HtmlEditorAsset::register($view);
    }

    /**
     * @inheritdoc
     */
    public function run()
	{
        if ($this->hasModel()) {
            return Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            return Html::textarea($this->name, $this->value, $this->options);
        }
	}
}
