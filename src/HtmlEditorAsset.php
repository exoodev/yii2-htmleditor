<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\htmleditor;

/**
 * Asset bundle for widget [[HtmlEditor]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class HtmlEditorAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/htmleditor/assets';
    /**
     * @inheritdoc
     */
    public $js = [
        'js/htmleditor.js',
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'css/htmleditor.css',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        'exoo\kit\ExookitAsset',
        'exoo\codemirror\CodemirrorAsset',
    ];
}
