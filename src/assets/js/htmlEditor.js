//https://developer.mozilla.org/ru/docs/Web/API/Document/execCommand
(function($) {
    jQuery.fn.htmlEditor = function(options) {

        var defaults = {
            template: [
                '<div class="ex-htmleditor uk-clearfix">',
                '<div class="ex-htmleditor-navbar">',
                '<ul class="ex-htmleditor-navbar-nav ex-htmleditor-toolbar"></ul>',
                '<div class="ex-htmleditor-navbar-flip">',
                '<ul class="ex-htmleditor-navbar-nav">',
                '<li class="ex-htmleditor-button-text"><a>{:lblText}</a></li>',
                '<li class="ex-htmleditor-button-code"><a>{:lblCode}</a></li>',
                '<li class="ex-htmleditor-button-fullscreen"><a><i class="fas fa-expand"></i></a></a></li>',
                '</ul>',
                '</div>',
                '</div>',
                '<div class="ex-htmleditor-content">',
                '<div class="ex-htmleditor-text"></div>',
                '<div class="ex-htmleditor-code"></div>',
                '</div>',
                '</div>'
            ].join(''),
            codemirror: {
                mode: 'htmlmixed',
                lineWrapping: true,
                dragDrop: false,
                autoCloseTags: true,
                matchTags: true,
                autoCloseBrackets: true,
                matchBrackets: true,
                indentUnit: 4,
                indentWithTabs: false,
                tabSize: 2,
                hintOptions: {completionSingle:false}
            },
            element: null,
            height: 300,
            toolbar: {
                text: ['bold', 'italic', 'underline', 'strikethrough', 'olist', 'ulist', 'link', 'image', 'justifyLeft', 'justifyCenter', 'justifyRight', 'h2', 'h3', 'removeFormat'],
                code: [],
            },
            iframe: {
                css: ['https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.24/css/uikit.min.css'],
                js: [
                    'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.24/js/uikit.min.js',
                    'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.24/js/uikit-icons.min.js'
                ]
            },
            storage: false,
            labels: {
                ok: 'OK',
                cancel: 'Отмена',
                select: 'Выбрать',
                insert: 'Вставить',
                new_tab: 'Открыть в новой вкладке',
                text: 'Текст',
                code: 'Код',
                insert_link: 'Создание ссылки',
                image: 'Изображение',
                width: 'Ширина',
                height: 'Высота',
                alt: 'Альтернативный текст',
                size: 'Размер (px)'
            },
            actions: {
                bold: {
                    icon: '<i class="fas fa-bold"></i>',
                    title: 'Жирный',
                    result: function() { return exec('bold') }
                },
                italic: {
                    icon: '<i class="fas fa-italic"></i>',
                    title: 'Наклонный',
                    result: function() { return exec('italic') }
                },
                underline: {
                    icon: '<i class="fas fa-underline"></i>',
                    title: 'Подчеркнутый',
                    result: function() { return exec('underline') }
                },
                strikethrough : {
                    icon: '<i class="fas fa-strikethrough"></i>',
                    title: 'Зачеркнутый',
                    result: function() { return exec('strikethrough') }
                },
                olist: {
                    icon: '<i class="fas fa-list-ol"></i>',
                    title: 'Нумерованный список',
                    result: function() { return exec('insertOrderedList') }
                },
                ulist: {
                    icon: '<i class="fas fa-list-ul"></i>',
                    title: 'Список',
                    result: function() { return exec('insertUnorderedList') }
                },
                link: {
                    icon: '<i class="fas fa-link"></i>',
                    title: 'Ссылка',
                    result: function() {
                        var data = {},
                        modal = UIkit.modal.dialog(([
                            '<div class="uk-modal-body">',
                            '<h3>'+options.labels.insert_link+'</h3>',
                            '<form class="uk-form uk-form-stacked">',
                            '<div class="uk-margin">',
                            '<label class="uk-form-label">URL</label>',
                            '<div class="uk-form-controls">',
                            '<input type="text" name="url" class="uk-input">',
                            '</div>',
                            '</div>',
                            '<div class="uk-margin">',
                            '<label class="uk-form-label">'+options.labels.text+'</label>',
                            '<div class="uk-form-controls">',
                            '<input type="text" name="text" class="uk-input">',
                            '</div>',
                            '</div>',
                            '<div class="uk-margin">',
                            '<div class="uk-form-controls">',
                            '<input id="form-s-blank" class="uk-checkbox" type="checkbox" name="blank">',
                            '<label for="form-s-blank" class="uk-margin-small-left">'+options.labels.new_tab+'</label>',
                            '</div>',
                            '</div>',
                            '</form>',
                            '</div>',
                            '<div class="uk-modal-footer uk-text-right">',
                            '<button class="uk-button uk-modal-close">'+options.labels.cancel+'</button> ',
                            '<button class="uk-button uk-button-primary js-modal-ok">'+options.labels.insert+'</button>',
                            '</div>'
                        ]).join(""), {center: true});
                        $(modal.$el).find('.js-modal-ok').on('click', function() {
                            $(modal.$el).find('form').serializeArray().map(function(e) {
                                data[e.name] = e.value;
                            });
                            if (data.url && data.text) {
                                modal.hide();
                                var blank = data.blank ? ' target="_blank"' : '';
                                return exec('insertHTML', '<a href="'+data.url+'"'+blank+'>'+data.text+'</a>');
                            }
                        });
                    }
                },
                image: {
                    icon: '<i class="fas fa-image"></i>',
                    title: 'Изображение',
                    result: function() {
                        var data = {},
                        modal = UIkit.modal.dialog(([
                            '<div class="uk-modal-body">',
                            '<h3>'+options.labels.image+'</h3>',
                            '<form class="uk-form uk-form-stacked">',
                            '<div class="uk-margin">',
                            '<label class="uk-form-label">URL</label>',
                            '<div class="uk-form-controls">',
                            '<input id="ex-htmleditor-image" type="text" name="url" class="uk-input">',
                            '</div>',
                            '</div>',
                            '<div class="uk-margin">',
                            '<label class="uk-form-label">'+options.labels.alt+'</label>',
                            '<div class="uk-form-controls">',
                            '<input type="text" name="alt" class="uk-input">',
                            '</div>',
                            '</div>',
                            '<div class="uk-margin">',
                            '<label class="uk-form-label">'+options.labels.size+'</label>',
                            '<div class="uk-form-controls uk-flex">',
                            '<input class="uk-input" type="number" name="width" placeholder="'+options.labels.width+'" min="0"> ',
                            '<input class="uk-input" type="number" name="height" placeholder="'+options.labels.height+'" min="0">',
                            '</div>',
                            '</div>',
                            '</form>',
                            '</div>',
                            '<div class="uk-modal-footer uk-text-right">',
                            // '<button class="uk-button uk-button-primary uk-float-left" data-ex-media="{target: \'#ex-htmleditor-image\', link: false}">'+options.labels.select+'</button> ',
                            '<button class="uk-button uk-modal-close">'+options.labels.cancel+'</button> ',
                            '<button class="uk-button uk-button-primary js-modal-ok">'+options.labels.insert+'</button>',
                            '</div>'
                        ]).join(""), {center: true, modal: false});
                        $(modal.$el).on('click', '.js-modal-ok', function() {
                            $(modal.$el).find('form').serializeArray().map(function(e) {
                                data[e.name] = e.value;
                            });
                            if (data.url) {
                                modal.hide();
                                var width = data.width ? ' width="'+data.width+'"' : '';
                                var height = data.height ? ' height="'+data.height+'"' : '';
                                return exec('insertHTML', '<img src="'+data.url+'" alt="'+data.alt+'"'+width+height+'>');
                            }
                        });
                    }
                },
                justifyLeft: {
                    icon: '<i class="fas fa-align-left"></i>',
                    title: 'Выравнивание по левому краю',
                    result: function() { return exec('justifyLeft') }
                },
                justifyRight: {
                    icon: '<i class="fas fa-align-right"></i>',
                    title: 'Выравнивание по правому краю',
                    result: function() { return exec('justifyRight') }
                },
                justifyCenter: {
                    icon: '<i class="fas fa-align-center"></i>',
                    title: 'Выравнивание по центру',
                    result: function() { return exec('justifyCenter') }
                },
                h2: {
                    icon: '<b>H2</b>',
                    title: 'Заголовок H2',
                    result: function() { return exec('formatBlock', '<H2>') }
                },
                h3: {
                    icon: '<b>H3</b>',
                    title: 'Заголовок H3',
                    result: function() { return exec('formatBlock', '<H3>') }
                },
                removeFormat: {
                    icon: '<i class="fas fa-eraser"></i>',
                    title: 'Очистить форматирование',
                    result: function() { return exec('removeFormat') }
                },
            }
        }

        var options = $.extend({}, defaults, options || {});

        function _render(type) {
            if (type) {
                localStorage.setItem('htmleditor', type);
            } else {
                type = _currentType();
            }

            options.element.text.toggle(type == 'text');
            options.element.code.toggle(type == 'code');
            $('.ex-htmleditor-button-text').toggleClass('ex-active', type == 'text');
            $('.ex-htmleditor-button-code').toggleClass('ex-active', type == 'code');

            _buildToolbar(type);

            setTimeout(function() {
                options.element.codeMirror.refresh();
            }, 50);
        }

        function _currentType() {
            return localStorage.getItem('htmleditor') || 'text';
        }

        function _buildToolbar(type) {
            var toolbar = options.toolbar[type];
            options.element.toolbar.empty();

            if (!(toolbar && toolbar.length)) return;
            var buttons = [];

            toolbar.forEach(function(button) {
                if (!options.actions[button]) return;

                var title = options.actions[button].title || button;

                buttons.push('<li><a data-htmleditor-button="'+button+'" title="'+title+'" data-uk-tooltip>'+options.actions[button].icon+'</a></li>');
            });

            options.element.toolbar.html(buttons.join('\n'));
        }

        function exec(command) {
            var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var el = options.element,
            doc = el.iframe[0].contentWindow.document;
            doc.execCommand(command, false, value);

            //fix removeFormat for headings H1,H2...
            if (command === 'removeFormat') {
                doc.execCommand('formatBlock', false, '<div>');
            }

            el.codeMirror.setValue(el.iframe.contents().find('body').html());
        };

        function _getValue() {
            var value = options.element.codeMirror.getValue();
            if (options.storage) {
                value = value.replace(/\{{storage}}/g, options.storage);
            }
            return value;
        }

        var init = function() {
            var template = options.template;
            var $this = this;

            template = template.replace(/\{:lblText}/g, options.labels.text);
            template = template.replace(/\{:lblCode}/g, options.labels.code);

            this.element = $(this);
            this.htmleditor = $(template);
            this.toolbar = this.htmleditor.find('.ex-htmleditor-toolbar');
            this.content = this.htmleditor.find('.ex-htmleditor-content');

            this.text = this.htmleditor.find('.ex-htmleditor-text');
            this.code = this.htmleditor.find('.ex-htmleditor-code');

            this.text.css('height', options.height);

            this.element.before(this.htmleditor).appendTo(this.code);
            this.codeMirror = CodeMirror.fromTextArea(this.element[0], options.codemirror);
            this.code.find('.CodeMirror').css('height', options.height);

            options.element = this;
            this.iframe = $('<iframe class="ex-htmleditor-iframe" frameborder="0" scrolling="auto" width="100%"></iframe>');
            this.iframe.attr('height', options.height);
            this.text.append(this.iframe);

            this.iframe.ready(function() {
                var $body = $this.iframe.contents().find('body');
                var $head = $this.iframe.contents().find('head');

                $body.css('padding', '10px');
                $head.append('<style type="text/css">.ex-htmleditor-overlay {opacity:.5}img{max-width:none}</style>');
                $this.iframe[0].contentWindow.document.designMode = 'on';
                if ($this.codeMirror.options.mode == 'htmlmixed') {
                    $.each(options.iframe.css, function(index, url) {
                        $head.append('<link rel="stylesheet" href="'+url+'">');
                    });
                    // $.each(options.iframe.js, function(index, url) {
                    //     $head.append('<scr' + 'ipt type="text/javascript" src="'+url+'"></scr' + 'ipt>');
                    // });
                }
                $body.append(_getValue());



                $body.on('keyup change paste', function(e) {
                    $this.codeMirror.setValue(e.target.innerHTML);
                });

                //active image
                $body.on('click', function(e) {
                    if (!$(e.target).is('img')) {
                        $body.find('img').removeClass('ex-htmleditor-overlay');
                    } else {
                        $(e.target).addClass('ex-htmleditor-overlay');
                    }
                });

                // $body.on('keypress', function(e) {
                //     if(e.keyCode == '13')
                //         document.execCommand('formatBlock', false, 'p');
                // });
            });

            this.htmleditor.on('click', '.ex-htmleditor-button-text a', function(e) {
                e.preventDefault();
                $this.iframe.contents().find('body').html(_getValue());
                _render('text');
            });

            this.htmleditor.on('click', '.ex-htmleditor-button-code a', function(e) {
                e.preventDefault();
                _render('code');
                $this.codeMirror.refresh();
            });

            this.htmleditor.on('click', '.ex-htmleditor-button-fullscreen a', function(e) {
                e.preventDefault();

                if ($this.htmleditor.css('position') != 'fixed') {
                    $this.codeMirror.state.restoreHeight = $this.text.height();
                }

                $this.htmleditor.toggleClass('ex-htmleditor-fullscreen');

                if ($this.htmleditor.hasClass('ex-htmleditor-fullscreen')) {
                    $this.text.css('height', $this.content.height() + 'px');
                    document.documentElement.style.overflow = 'hidden';
                } else {
                    $this.text.css('height', $this.codeMirror.state.restoreHeight + 'px');
                    document.documentElement.style.overflow = '';
                }
            });

            $('[uk-switcher], [uk-tab]').on('click', function(e) {
                e.stopImmediatePropagation();
                setTimeout(function() {
                    var editors = $('.uk-switcher .uk-active').find('.CodeMirror');
                    editors.each(function(index, editor) {
                        editor = $(editor)[0]
                        if (editor && editor.CodeMirror) {
                            editor.CodeMirror.refresh();
                        }
                    })
                }, 500);
            });

            // toolbar actions
            this.htmleditor.on('click', 'a[data-htmleditor-button]', function() {
                var action = $(this).data('htmleditor-button');
                var type = _currentType();

                if (type == 'code') {

                } else if (type == 'text') {
                    if (options.actions[action]) {
                        options.actions[action].result();

                    }
                }
            });

            _render();
        };

        return this.each(init);
    };
})(jQuery);
